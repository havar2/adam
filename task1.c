// function in C that takes a string and converts it to upper case.
#include <stdio.h>

// This function gets the string as a pointer and change the characters one by one:
int toUpperCaseFunc(char *string_in)
{
    while (*string_in != '\0')                          /* run until the NULL characer */
    {
        if (*string_in >= 'a' && *string_in <= 'z')     /* check if the character is in a to z lowercase */
        {
            *string_in = *string_in - ('a' - 'A');      /* change the character according the ASCII table */
        }
        string_in++;                                    /* go to the next character by incrimenting the address */
    }
    return 0;
}

// to run the function for testing:
int main(void)
{
    char inputStr[] = "Havaaaaaaar BatHaeeeeee";
    
    printf("Original text: %s\n", inputStr);
    
    toUpperCaseFunc(inputStr);                          /* run the funciton */
    
    printf("Changed text: %s\n", inputStr);
    
    return 0;
}

// I copied and compiled this code on my machine with gcc and it works.
