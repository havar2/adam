// function to encode an arbitrary 32-bit unsigned integer as a sysex message and print that message.
#include <stdio.h>
#include <stdint.h>

// #define ARRAY_SIZE 6 /* size of the message array for simple encode*/
#define ARRAY_SIZE 7 /* size of the message array for 7bits encode*/

// Encode function:
// This function gets two arguments, the 32-bit unsigned integer, and a pointer to the array of the message. 
// First it encodes the message and saves it in the array. Then it prints the resault.
int sysexEncoder(uint32_t number_in, uint8_t sysex[])
{
    uint8_t buff = 0;
    
    sysex[0] = 0xF0;        /* start of the message */
    
    // Simple encode: MSB=0
    // This way the number is changed to follow the rule for the MSB: 0
    // Since I'm not sure what exactly requested I will change this part of the code so it will take 7 bits at a time.
    /*for (int i = 3; i>=0; i--)
    {
        buff = (number_in >> (i*8)) & 0xFF;
        buff &= 0x7F;
        sysex[4-i] = buff;
    }*/
    
    // 7-bit encode: 7bit at a time:
    // puts every 7-bit of the number in a message byte so MSB of the byte is 0
    for (int i = 4; i>=0; i--)
    {
        buff = (number_in >> (i*7)) & 0x7F;
        sysex[5-i] = buff;
    }
    
    sysex[ARRAY_SIZE -1] = 0xF7;        /* end of the message */
    
    // Printing the message:
    printf("Encoded Sysex Message: ");
    for(int i = 0; i < ARRAY_SIZE; i++)
        printf("%02X ", sysex[i]);
    printf("\n");
}

// Decoding a Sysex message into a uint32_t number:
int sysexDecoder(uint8_t sysex[])
{
    uint32_t number_out = 0;
    
    if (sysex[0] != 0xF0 || sysex[ARRAY_SIZE-1] != 0xF7) return -1;   /* check for the header and footer of the message */
    
    for (int i = 4; i>=0; i--)
    {
        number_out <<= 7;
        number_out |= (sysex[5-i] & 0x7F);
    }
    
    return number_out;
}

int main(void)
{
    uint32_t num = 123456789;
    // uint32_t num = 4294967294; // Example unsigned integer 
    uint32_t num_out = 0;
    uint8_t sysexMsg[ARRAY_SIZE] = {0};
    
    printf("Number: %u\n", num);        /* print the number to be encoded */
    
    sysexEncoder(num, sysexMsg);        /* encode the message and print it */
    
    num_out = sysexDecoder(sysexMsg);   /* decode the message and make the number */
    
    if (num == -1)                      /* check if the decoding was successfull */
        printf("ERROR: invalid message");
    else
        printf("Decodec number: %u\n",num_out);     /* print the decoded number */
    
    if(num_out != num)
        printf("Encode/Decode unsuccessful!\n");
    else
        printf("Encode/Decode successful!\n");
    return 0;
}

// I just tested the program, it compiles and runs well.
// I also checked with the maximum unsigned int number.
